## This package implements the Fowler-Noll-Vo hash function,
## revisions 1 and 1A (usually called FNV-1 and FNV-1a,
## respectively). The 32, 64, and 128-bit variants of both
## functions are currently available.
##
## FNV-0 is not included in this package, as it is deprecated
## and highly ineffective in comparison to its counterparts.
##
## The FNV hash functions are non-cryptographic. They should
## not be used for anything where security is of concern.
##
## 128-bit hashes in this library use the stint_ nimble package.
##
## .. _stint: https://github.com/status-im/nim-stint
##

import stint

const
  prime32 = 16777619'u32
  prime64 = 1099511628211'u64
  prime128 = "309485009821345068724781371".u128
  offset32 = 2166136261'u32
  offset64 = 14695981039346656037'u64
  offset128 = "144066263297769815596495629667062367629".u128

type
  FnvHashable* = concept x
    ## Concept over any type that can be used to generate
    ## an FNV hash. Anything that can be iterated over in
    ## a for loop, giving elements that can be casted to
    ## uint8, will fulfill this concept. This includes
    ## strings, arrays, openArrays, seqs, etc. as long
    ## as the elements can be casted to uint8.
    for c in x:
      c.uint8 is uint8


func fnv1a_32*(x: FnvHashable): uint32 =
  ## Produce a 32-bit FNV-1a hash.
  ## x is of the concept FnvHashable, described above.
  result = offset32
  for b in x:
    result = result xor b.uint8
    result = result * prime32


func fnv1a_64*(x: FnvHashable): uint64 =
  ## Produce a 64-bit FNV-1a hash.
  ## x is of the concept FnvHashable, described above.
  result = offset64
  for b in x:
    result = result xor b.uint8
    result = result * prime64

func fnv1a_128*(x: FnvHashable): UInt128 =
  ## Produce a 128-bit FNV-1 hash. The return type is
  ## 'UInt128' from the stint_ nimble package.
  ##
  ## .. _stint: https://github.com/status-im/nim-stint
  result = offset128
  for b in x:
    let bu = b.uint8
    # HACK: for some reason, the combination of package
    # imports, the FnvHashable concept, and stint internals
    # makes errors get thrown if bu isn't cast to a string
    # before being turned into a u128.
    result = result xor u128($bu)
    result = result * prime128


func fnv1_32*(x: FnvHashable): uint32 =
  ## Produce a 32-bit FNV-1 hash.
  ## x is of the concept FnvHashable, described above.
  result = offset32
  for b in x:
    result = result * prime32
    result = result xor b.uint8


func fnv1_64*(x: FnvHashable): uint64 =
  ## Produce a 64-bit FNV-1 hash.
  ## x is of the concept FnvHashable, described above.
  result = offset64
  for b in x:
    result = result * prime64
    result = result xor b.uint8

func fnv1_128*(x: FnvHashable): UInt128 =
  ## Produce a 128-bit FNV-1 hash. The return type is
  ## 'UInt128' from the stint_ nimble package.
  ##
  ## .. _stint: https://github.com/status-im/nim-stint
  result = offset128
  for b in x:
    let bu = b.uint8
    result = result * prime128
    # HACK see fnv1a_128
    result = result xor u128($bu)
