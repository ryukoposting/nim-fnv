# Package

version       = "0.1.0"
author        = "ryukoposting"
description   = "FNV-1 and FNV-1a non-cryptographic hash functions (documentation hosted at: http://ryuk.ooo/nimdocs/fnv/fnv.html)"
license       = "Apache-2.0"
srcDir        = "src"


# Dependencies

requires "nim >= 0.19.0"
requires "stint"

task docs, "generate documentation":
  exec "nim doc src/fnv.nim"
