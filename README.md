```nim
import fnv
```

**[Full documentation is hosted here.](http://ryuk.ooo/nimdocs/fnv/fnv.html)**

`fnv` implements the 32- and 64-bit variants of the FNV-1 and FNV-1a
hash functions. Using them is simple:

```nim
let
  hash1 = fnv1a_64("hello, world!")
  hash2 = fnv1_32(['a', 'r', 'r', 'a', 'y'])
  hash3 = fnv1a_32(@['s', 'e', 'q', 'u', 'e', 'n', 'c', 'e'])

# thanks to style insensitivity, the above is the same as:
let
  hash1 = fnv1a64("hello, world!")
  hash2 = fnv132(['a', 'r', 'r', 'a', 'y'])
  hash3 = fnv1a32(@['s', 'e', 'q', 'u', 'e', 'n', 'c', 'e'])
```

The functions implemented here should work for anything that
can be looped over in a for loop, whose elements can be casted
to `uint8`.
