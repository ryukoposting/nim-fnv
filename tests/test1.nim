import unittest
import stint

import fnv

test "strings 1A":
  check fnv1a_64("hello world") == 8618312879776256743u64
  check fnv1a_32("hello world") == 3582672807u32
  check fnv1a_128("hello world") == "143667438548887148232425432707801491127".u128


test "arrays 1A":
  let helloArr = ['h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd']
  check fnv1a_64(helloArr) == fnv1a_64("hello world")
  check fnv1a_32(helloArr) == fnv1a_32("hello world")
  check fnv1a_128(helloArr) == fnv1a_128("hello world")

test "seqs 1A":
  let helloSeq = @['h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd']
  check fnv1a_64(helloSeq) == fnv1a_64("hello world")
  check fnv1a_32(helloSeq) == fnv1a_32("hello world")
  check fnv1a_128(helloSeq) == fnv1a_128("hello world")

test "strings 1":
  check fnv1_64("hello world") == 9065573210506989167u64
  check fnv1_32("hello world") == 1418570095u32
  check fnv1_128("hello world") == "299997385309276116067819595102309100575".u128


test "arrays 1":
  let helloArr = ['h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd']
  check fnv1_64(helloArr) == fnv1_64("hello world")
  check fnv1_32(helloArr) == fnv1_32("hello world")
  check fnv1_128(helloArr) == fnv1_128("hello world")

test "seqs 1":
  let helloSeq = @['h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd']
  check fnv1_64(helloSeq) == fnv1_64("hello world")
  check fnv1_32(helloSeq) == fnv1_32("hello world")
  check fnv1_128(helloSeq) == fnv1_128("hello world")
